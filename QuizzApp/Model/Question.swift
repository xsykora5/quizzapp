//
//  Question.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 17.03.2024.
//

import Foundation


struct Question : Codable, Identifiable, Equatable {
    var id = UUID()
    var text: String
    var image: String?
    var answer: ClosedQuestionAnswer = .A
    var questionOptions: [QuestionOption] = [];
}

enum ClosedQuestionAnswer : String, Codable, CaseIterable {
    case A, B, C, D
}
