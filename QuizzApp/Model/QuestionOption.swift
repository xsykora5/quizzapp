//
//  QuestionOption.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 28.04.2024.
//

import Foundation

struct QuestionOption : Identifiable, Codable, Hashable {
    var id = UUID()
    var questionId: UUID
    var text: String
    var option: ClosedQuestionAnswer
}
