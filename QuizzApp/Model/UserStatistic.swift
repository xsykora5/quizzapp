//
//  UserStatistic.swift
//  QuizzApp
//
//  Created by Marek Seďa on 18.05.2024.
//

import Foundation

struct UserStatistic {
    let numberOfCompletedQuizes: Int;
    let score: Int;
    let numberOfCreatedQuizes: Int;
}
