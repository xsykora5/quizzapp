//
//  QuizCompleted.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 29.04.2024.
//

import Foundation



struct QuizCompleted: Codable {
    let quizId: String?;
    let playerId: String?;
    let score: Int;
}
