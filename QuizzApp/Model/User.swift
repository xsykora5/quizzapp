//
//  User.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 12.05.2024.
//

import Foundation
import FirebaseFirestore


struct User : Codable, Identifiable {
    @DocumentID var id: String?
    var name: String;
    var email: String;
    var score: Int;
}
