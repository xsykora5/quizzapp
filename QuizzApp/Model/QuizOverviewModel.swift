//
//  QuizOverviewModel.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 16.03.2024.
//

import Foundation
import SwiftUI


struct QuizOverviewModel : Identifiable {
    let id = UUID()
    let name: String
    let color: Color;
    let icon: String
    let numQuestions: Int
    let difficulty: String
    let topic: String
    let author: String
}

let sampleQuizzes: [QuizOverviewModel] = [
    QuizOverviewModel(name: "Quiz 1", color: .red, icon: "star", numQuestions: 10, difficulty: "Easy", topic: "Swift", author: "John Doe"),
       QuizOverviewModel(name: "Quiz 2", color: .blue, icon: "square", numQuestions: 15, difficulty: "Medium", topic: "iOS", author: "Jane Smith"),
       QuizOverviewModel(name: "Quiz 3", color: .green, icon: "circle", numQuestions: 12, difficulty: "Hard", topic: "Programming", author: "Alex Johnson"),
       QuizOverviewModel(name: "Quiz 4", color: .orange, icon: "triangle", numQuestions: 8, difficulty: "Easy", topic: "Technology", author: "John Doe"),
       QuizOverviewModel(name: "Quiz 5", color: .yellow, icon: "bolt", numQuestions: 18, difficulty: "Medium", topic: "Swift", author: "Jane Smith"),
       QuizOverviewModel(name: "Quiz 6", color: .red, icon: "heart", numQuestions: 20, difficulty: "Hard", topic: "iOS", author: "Alex Johnson"),
       QuizOverviewModel(name: "Quiz 7", color: .blue, icon: "flag", numQuestions: 14, difficulty: "Easy", topic: "Programming", author: "John Doe"),
       QuizOverviewModel(name: "Quiz 8", color: .green, icon: "moon", numQuestions: 17, difficulty: "Medium", topic: "Technology", author: "Jane Smith"),
       QuizOverviewModel(name: "Quiz 9", color: .orange, icon: "play", numQuestions: 11, difficulty: "Hard", topic: "Swift", author: "Alex Johnson"),
       QuizOverviewModel(name: "Quiz 10", color: .yellow, icon: "cloud", numQuestions: 9, difficulty: "Easy", topic: "iOS", author: "John Doe"),
       QuizOverviewModel(name: "Quiz 11", color: .red, icon: "house", numQuestions: 16, difficulty: "Medium", topic: "Programming", author: "Jane Smith"),
       QuizOverviewModel(name: "Quiz 12", color: .blue, icon: "car", numQuestions: 13, difficulty: "Hard", topic: "Technology", author: "Alex Johnson"),
       QuizOverviewModel(name: "Quiz 13", color: .green, icon: "book", numQuestions: 19, difficulty: "Easy", topic: "Swift", author: "John Doe"),
       QuizOverviewModel(name: "Quiz 14", color: .orange, icon: "pencil", numQuestions: 7, difficulty: "Medium", topic: "iOS", author: "Jane Smith"),
       QuizOverviewModel(name: "Quiz 15", color: .yellow, icon: "person", numQuestions: 11, difficulty: "Hard", topic: "Programming", author: "Alex Johnson"),
       QuizOverviewModel(name: "Quiz 16", color: .red, icon: "bicycle", numQuestions: 15, difficulty: "Easy", topic: "Technology", author: "John Doe"),
       QuizOverviewModel(name: "Quiz 17", color: .blue, icon: "airplane", numQuestions: 20, difficulty: "Medium", topic: "Swift", author: "Jane Smith"),
       QuizOverviewModel(name: "Quiz 18", color: .green, icon: "globe", numQuestions: 10, difficulty: "Hard", topic: "iOS", author: "Alex Johnson"),
       QuizOverviewModel(name: "Quiz 19", color: .orange, icon: "leaf", numQuestions: 16, difficulty: "Easy", topic: "Programming", author: "John Doe"),
       QuizOverviewModel(name: "Quiz 20", color: .yellow, icon: "flame", numQuestions: 14, difficulty: "Medium", topic: "Technology", author: "Jane Smith")

]
