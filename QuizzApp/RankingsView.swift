//
//  Rankings.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 15.03.2024.
//

import SwiftUI
import FirebaseFirestore

struct PlayerRow: View {
    let user: User
    
    var body: some View {
        HStack {
            Image(systemName: "person")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 50, height: 50)
                .clipShape(Circle())
            VStack(alignment: .leading) {
                Text(user.name)
                    .font(.headline)
                Text("Score: \(user.score)")
                    .font(.subheadline)
                    .foregroundColor(.gray)
            }
            Spacer()
        }
        .padding(.vertical, 8)
    }
}



struct RankingsView: View {
    @FirestoreQuery(collectionPath: "users") var users: [User];
    
    var body: some View {
        NavigationView {
            List {
                ForEach(users.sorted(by: {$0.score > $1.score})) { user in
                    PlayerRow(user: user)
                }
            }
            .navigationTitle("Rankings")
        }
    }
}
