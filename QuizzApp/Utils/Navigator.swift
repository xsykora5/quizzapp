//
//  Navigator.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 17.05.2024.
//

import Foundation
import Observation
import SwiftUI

@Observable
class NavigationCoordinator {
    var paths = NavigationPath()
    
    @ViewBuilder
    func navigate(to screen: Screen) -> some View {
        
        switch screen {
        case .homeScreen:
            HomeView();
        case .createNewScreen:
            CreateNewQuizView();
        case .quizDetail(let quiz):
            QuizSummaryView(quiz: quiz)
        case .quizGame(let quiz):
            GameView(quiz: quiz)
        case .quizFinished(let params):
            QuizCompletedView(quiz: params.quiz, rightAnswers: params.rightAnswers)
        case .updateQuizView(let quiz):
            UpdateQuizView(quiz: quiz, name: quiz.name, description: quiz.description, topic: quiz.topic, difficulty: quiz.difficulty, bgColor: quiz.color, questions: quiz.questions)
        case .playerQuizzes(let playerId):
            PlayerQuizzesListView(author: playerId)
        case .editQuestion(let params):
            UpdateQuestionView(optionAText: params.$optionAText, optionBText: params.$optionBText, optionCText: params.$optionCText, optionDText: params.$optionDText, questionText: params.$questionText, answer: params.$answer)
        default:
            FallbackView()
        }
    }
    
    func push(_ screen: Screen) {
        paths.append(screen)
    }
    
    func pop() {
        paths.removeLast()
    }
    
    func popToRoot() {
        paths.removeLast(paths.count)
    }
}

struct NewScreenParams {
    
}

struct QuizDetailParams {
    var quiz: Quiz
}
