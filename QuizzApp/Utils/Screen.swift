//
//  Screen.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 17.05.2024.
//

import Foundation
import SwiftUI

enum Screen {
    case homeScreen
    case createNewScreen
    case quizDetail(params: Quiz)
    case quizGame(params: Quiz)
    case quizFinished(params: QuizFinshedParams)
    case updateQuizView(params: Quiz)
    case playerQuizzes(params: String)
    case editQuestion(params: EditQuestionParams)
}


extension Screen: Hashable {
    public func hash(into hasher: inout Hasher) {
        switch self {
        case .quizDetail(let value):
            hasher.combine(value.id)
        case .quizGame(let value):
            hasher.combine(value.id)
        case .quizFinished(let value):
            hasher.combine(value.quiz.id)
        case .updateQuizView(let value):
            hasher.combine(value.id)
        case .playerQuizzes(let value):
            hasher.combine(value)
        case .editQuestion(let value):
            hasher.combine(value.id)
        default:
            break
        }
    }
}

extension Screen : Equatable {
    static func == (lhs: Screen, rhs: Screen) -> Bool {
        return lhs.hashValue == rhs.hashValue;
    }
}

struct QuizFinshedParams : Equatable {
    let quiz : Quiz
    let rightAnswers: Int
}


struct EditQuestionParams {
    var id : String;
    @Binding var optionAText : String;
    @Binding var optionBText : String;
    @Binding var optionCText : String;
    @Binding var optionDText : String;
    @Binding var questionText : String;
    @Binding var answer : ClosedQuestionAnswer;
}
