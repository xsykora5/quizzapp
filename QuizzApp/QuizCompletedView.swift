//
//  QuizCompletedView.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 17.05.2024.
//

import SwiftUI

struct QuizCompletedView: View {
    @Environment(NavigationCoordinator.self) var coordinator: NavigationCoordinator
    
    var quiz: Quiz;
    var rightAnswers: Int;
    var body: some View {
        VStack(alignment: .center) {
            Text("\(quiz.name) completed!")
                .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                .fontWeight(.bold)
                .multilineTextAlignment(.center)
                .foregroundStyle(.indigo)
                .padding()
            Spacer()
            Text("Score")
                .font(.title3)
            Spacer().frame(height: 20)
            Text("\(rightAnswers)/\(quiz.numQuestions)")
                .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                .fontWeight(.bold)
                .multilineTextAlignment(.center)
                .foregroundStyle(.indigo)
            
            Spacer().frame(height: 20)
            Text("+\(calculateScore(difficulty: quiz.difficulty, rightAnswers: rightAnswers)) points")
            Spacer()
            Button {
                Task {
                    let user = await serviceContext.databaseService.getLoggedUser()
                    let quizCompleted = QuizCompleted(quizId: quiz.id, playerId: user.id, score: calculateScore(difficulty: quiz.difficulty, rightAnswers: rightAnswers))
                    await serviceContext.databaseService.completeQuiz(quizCompleted: quizCompleted);
                }
                
                coordinator.popToRoot()
            } label: {
                Text("Collect points").foregroundStyle(.indigo).font(.title2)
            }

        }
    }
}

func calculateScore(difficulty: Difficulty, rightAnswers: Int) -> Int {
    var modifier = 1;
    switch difficulty {
    case .EASY:
        modifier = 1;
        break
    case .MEDIUM:
        modifier = 2;
        break;
    case .HARD:
        modifier = 3;
        break;
    }
    
    return modifier * 50 * rightAnswers;
}
