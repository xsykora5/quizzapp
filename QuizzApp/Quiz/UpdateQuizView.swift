//
//  UpdateQuizView.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 18.05.2024.
//

import SwiftUI
import SFSymbolsPicker

struct UpdateQuizView: View {
    @Environment(NavigationCoordinator.self) var coordinator: NavigationCoordinator

    @State var activeSheet: Bool = false;
    @State var isPresented: Bool = false;
    
    var quiz : Quiz;
    @State var name: String = ""
    @State var description: String = ""
    @State var topic: String = ""
    @State var color: String = "red"
    @State var difficulty: Difficulty = .EASY
    @State var bgColor = Color.red
    @State var questions : [Question] = []
    @State var showAlert: Bool = false;
    @State var icon : String = "star"
    
    var body: some View {
        VStack {
            Form {
                Section(header: Text("Quiz info")) {
                    TextField("Name", text: $name)
                    TextField("Description", text: $description)
                    TextField("Topic", text: $topic)
                    ColorPicker("Color", selection: $bgColor)
    
                    Picker("Difficulty", selection: $difficulty) {
                        ForEach(Difficulty.allCases, id: \.self) { difficulty in
                            Text(difficulty.rawValue.capitalized).tag(difficulty.rawValue.capitalized)
                        }
                    }
                    
                    Button(action: {
                        isPresented.toggle()
                    }, label: {
                        HStack() {
                            Text("Pick an icon")
                            Spacer()
                            Image(systemName: icon)
                        }
                        .foregroundColor(.black)
                        .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/)
                    })
                    .sheet(isPresented: $isPresented, content: {
                        SymbolsPicker(selection: $icon, title: "Pick a symbol", autoDismiss: true) {
                            Text("Close")
                        }
                    })
                }
                QuestionListView(questions: $questions)
                
                Section {
                    HStack() {
                        Text("Add new question")
                    }
                    .foregroundStyle(.indigo)
                    .frame(maxWidth: .infinity)
                    .background(.white)
                    .cornerRadius(5.0)
                    .padding(.horizontal, 75)
                    
                    .onTapGesture {
                        activeSheet.toggle();
                    }
                }
            }
            Spacer()
        }
        .sheet(isPresented: $activeSheet) {
            AddQuestionSheetView(questions: $questions)
        }
        .toolbar {
            ToolbarItemGroup(placement: .primaryAction) {
                Button("Save") {
                    var updatedQuiz = quiz;
                    updatedQuiz.numQuestions = questions.count;
                    updatedQuiz.name = name;
                    updatedQuiz.questions = questions;
                    updatedQuiz.color = bgColor;
                    updatedQuiz.difficulty = difficulty;
                    updatedQuiz.description = description;
                    updatedQuiz.icon = icon;
                    updatedQuiz.topic = topic;
                    
                    let updated = serviceContext.databaseService.updateQuiz(quiz: updatedQuiz)
                    coordinator.pop()
                }
                
                Button("Cancel") {
                    coordinator.pop()
                }
                .foregroundStyle(.red)
            }
        }
    }
}
