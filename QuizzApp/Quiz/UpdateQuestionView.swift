//
//  UpdateQuestionView.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 19.05.2024.
//

import SwiftUI

struct UpdateQuestionView: View {
    @Environment(NavigationCoordinator.self) var coordinator: NavigationCoordinator

    @Binding var optionAText : String;
    @Binding var optionBText : String;
    @Binding var optionCText : String;
    @Binding var optionDText : String;
    @Binding var questionText : String;
    @Binding var answer : ClosedQuestionAnswer;

    var body: some View {
        Form {
            Section(header: Text("Add new question")) {
                TextField("Question text", text: $questionText)
                Text("Options")
                TextField("Option A text", text: $optionAText)
                TextField("Option B text", text: $optionBText)
                TextField("Option C text", text: $optionCText)
                TextField("Option D text", text: $optionDText)
                Text("Right answer")
                Picker("Right answer", selection: $answer) {
                    ForEach(ClosedQuestionAnswer.allCases, id: \.self) { option in
                        Text(option.rawValue).tag(option)
                    }
                }.pickerStyle(.segmented).colorMultiply(.indigo)
                Spacer()
                Button("Back") {
                    coordinator.pop()
                }
                .font(.title)
            }
            
        }
       
    }
}
