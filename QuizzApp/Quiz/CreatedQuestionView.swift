//
//  CreatedQuestionView.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 19.05.2024.
//

import SwiftUI

struct CreatedQuestionView: View {
    @Binding var question : Question
    var body: some View {
        
        VStack(alignment: .leading) {
            Text(question.text).fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
            
            
            ForEach(question.questionOptions, id: \.self) { option in
                
                let isRightAnswer = option.option == question.answer;
                HStack {
                    Text(option.option.rawValue).fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/).padding(.trailing, 5).foregroundStyle( isRightAnswer ? .indigo : .black)
                    if (isRightAnswer) {
                        Text(option.text).padding(.vertical, 5).fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/).foregroundStyle(.indigo)
                    } else {
                        Text(option.text).padding(.vertical, 5)
                    }
                }
                .frame(maxWidth: .infinity)
            }
        }
        .frame(maxWidth: .infinity)
        .padding()
        .background( RoundedRectangle(cornerRadius: 20.0)
            .fill(.white)
            .opacity(0.25)
            .shadow(radius: 10.0)
        )
    }
}
