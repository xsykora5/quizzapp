//
//  QuizSummaryView.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 17.03.2024.
//

import SwiftUI

struct QuizSummaryView: View {
    let quiz: Quiz
    var body: some View {

        let background_color = quiz.color;
        VStack(alignment: .leading) {
            RoundedRectangle(cornerRadius: 20.0)
                .fill(background_color)
                .frame(width: .infinity, height: 200)
                .overlay(
                    Image(systemName: quiz.icon)
                        .resizable()
                        .frame(width: 50, height: 50)
                        .foregroundColor(.white)
                        .padding()
                )
            Text(quiz.name)
                .font(.title)
                .fontWeight(.bold)
                .padding(.top)
                .foregroundStyle(.indigo)
            Text("by \(quiz.author)")
                .foregroundStyle(.gray)
            
            Grid {
                GridRow {
                    buildTile(image: "doc.text", info: "\(quiz.numQuestions)", caption: "questions")
                    buildTile(image: "gauge", info: "\(quiz.difficulty)", caption: "difficulty")
                    buildTile(image: "book", info: "\(quiz.topic)", caption: "topic")
                }
            }
            .padding(.bottom, 20)
            .frame(width: .infinity)
            .frame(maxHeight: 150)
            
            Text("Description")
                .font(.title3)
                .foregroundStyle(.indigo)
            Text(quiz.description)
            Spacer()
            
            NavigationLink(value: Screen.quizGame(params: quiz)) {
                HStack() {
                    Text("Jump In").padding()
                    Image(systemName: "arrow.right")
                }
                .foregroundStyle(.white)
                .frame(maxWidth: .infinity)
                .background(.indigo)
                .cornerRadius(5.0)
                .padding(.horizontal, 75)
            }
            Spacer()
            
        }.padding(.horizontal, 20)
    }
    
}
