//
//  AnswerSelectionComponent.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 13.05.2024.
//

import SwiftUI

struct AnswerSelectionComponent: View {
    var options: [QuestionOption]
    var rightAnswer: ClosedQuestionAnswer;
    
    @State private var selectedOption : ClosedQuestionAnswer? = nil;
    var body: some View {
        VStack(alignment: .leading, content: {
            ForEach(options, id: \.self) { option in
                var background = Color.gray;
//                if (option.option == selectedOption) {
//                    if (option.option == rightAnswer) {
//                        background = Color.green;
//                    } else {
//                        background = Color.red;
//                    }
//                }
                
                if (selectedOption != nil) {
                    VStack(alignment: .center) {
                        Text(option.text)
                            .frame(maxWidth: .infinity)
                            .padding()
                    }
                    .frame(width: .infinity)
                    .background(
                        
                    )
                    .onTapGesture {
                        selectedOption = option.option;
                    }
                } else {
                    VStack(alignment: .center) {
                        Text(option.text)
                            .frame(maxWidth: .infinity)
                            .padding()
                    }
                    .frame(width: .infinity)
                    .background(
                        RoundedRectangle(cornerRadius: 5)
                            .fill(.white)
                            .stroke(background, lineWidth: 1)
                    )
                    .onTapGesture {
                        selectedOption = option.option;
                    }
                }
            }
        })
        .frame(width: .infinity)
        .padding(.vertical, 20)
        .padding(.horizontal, 5)
    }
}
