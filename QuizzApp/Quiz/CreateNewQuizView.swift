//
//  CreateNewQuizView.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 16.03.2024.
//

import SwiftUI
import SFSymbolsPicker

enum ActiveSheet: Identifiable {
    case SAVED_QUIZ, NEW_QUESTION, SF_PICKER
    
    var id: Int {
        hashValue
    }
}

struct CreateNewQuizView: View {
    
    @State var name: String = ""
    @State var description: String = ""
    @State var topic: String = ""
    @State var color: String = "red"
    @State var difficulty: Difficulty = .EASY
    @State var bgColor = Color.red
    @State var questions : [Question] = []
    @State var icon : String = "star"
    @State var activeSheet : ActiveSheet?;
    @State var author : User? = nil;
    @State var showAlert: Bool = false;
    
    
    var body: some View {
        
        VStack {
            Form {
                Section(header: Text("Quiz info")) {
                    TextField("Name", text: $name)
                    TextField("Description", text: $description)
                    TextField("Topic", text: $topic)
                    ColorPicker("Color", selection: $bgColor)
                    Picker("Difficulty", selection: $difficulty) {
                        ForEach(Difficulty.allCases, id: \.self) { difficulty in
                            Text(difficulty.rawValue.capitalized).tag(difficulty.rawValue.capitalized)
                        }
                    }
                    Button(action: {
                        activeSheet = .SF_PICKER
                    }, label: {
                        HStack() {
                            Text("Pick an icon")
                            Spacer()
                            Image(systemName: icon)
                        }
                        .foregroundColor(.black)
                        .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/)
                    })
                }
                QuestionListView(questions: $questions)
                
                Section {
                    HStack() {
                        Text("Add new question")
                    }
                    .foregroundStyle(.indigo)
                    .frame(maxWidth: .infinity)
                    .background(.white)
                    .cornerRadius(5.0)
                    .padding(.horizontal, 75)
                    
                    .onTapGesture {
                        activeSheet = .NEW_QUESTION
                    }
                }
                
                Section {
                    HStack() {
                        Text("Create quiz").padding(.vertical, 10)
                    }
                    .foregroundStyle(.white)
                    .frame(maxWidth: .infinity)
                    .background(.indigo)
                    .cornerRadius(5.0)
                    .padding(.horizontal, 75)
                    .onTapGesture {
                        var quiz = Quiz(name: name, color: bgColor, icon: icon, numQuestions: questions.count, difficulty: difficulty, topic: topic, authorId: author!.id!, author: author!.name, description: description);
                        quiz.questions = questions;
                        
                        let questionFilled = quiz.questions.allSatisfy { question in
                            return !question.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty && question.questionOptions.allSatisfy{ option in !option.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty} && question.questionOptions.count == 4
                        }
                        if (quiz.name.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty || quiz.questions.isEmpty || quiz.topic.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty || quiz.description.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty || !questionFilled) {
                            showAlert = true;
                            return;
                        }
                        
                        let created = serviceContext.databaseService.addQuiz(quiz: quiz);
                        activeSheet = .SAVED_QUIZ
                        name = "";
                        description = "";
                        icon = "star"
                        topic = "";
                        color = "";
                        difficulty = .EASY;
                        questions = [];
                    }
                }
            }
            .onAppear() {
                Task {
                    author = await serviceContext.databaseService.getLoggedUser();
                }
            }
            .sheet(item: $activeSheet) { item in
                switch item {
                case .SAVED_QUIZ:
                    SavedQuizSheetView()
                case .NEW_QUESTION:
                    AddQuestionSheetView(questions: $questions)
                case .SF_PICKER:
                    SymbolsPicker(selection: $icon, title: "Pick a symbol", autoDismiss: true) {
                        Text("Close!")
                    }
                }
            }
            
            Spacer()
        }
        .alert(isPresented: $showAlert, content: {
            Alert(
                title: Text("Create quiz"),
                message: Text("Missing required information")
            )
        })
    }
}
