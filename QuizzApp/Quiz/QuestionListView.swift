//
//  QuestionListView.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 23.05.2024.
//

import SwiftUI

struct QuestionListView: View {
    @Environment(NavigationCoordinator.self) var coordinator: NavigationCoordinator
    
    @Binding var questions : [Question]
    var body: some View {
        List {
            ForEach($questions, id: \.id) { question in
                @State var options = question.questionOptions;
                CreatedQuestionView(question: question)
                    .onTapGesture {
                        var params = EditQuestionParams(id: question.id.uuidString,
                                                        optionAText: options[0].text,
                                                        optionBText: options[1].text,
                                                        optionCText: options[2].text,
                                                        optionDText: options[3].text,
                                                        questionText: question.text,
                                                        answer: question.answer)
                        
                        coordinator.push(.editQuestion(params: params))
                    }
            }
            .onDelete(perform: { indexSet in
                removeRows(at: indexSet)
            })
        }
    }
    
    func removeRows(at offsets: IndexSet) {
        questions.remove(atOffsets: offsets)
    }
}




