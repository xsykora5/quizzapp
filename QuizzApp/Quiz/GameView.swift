//
//  GameView.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 28.04.2024.
//

import SwiftUI

struct GameView: View {
    @Environment(NavigationCoordinator.self) var coordinator: NavigationCoordinator
    
    let quiz: Quiz;
    @State private var currentQuestion = 0;
    @State private var selectedOption : ClosedQuestionAnswer?
    @State private var rightAnswers : Int = 0;
    var body: some View {
        
        let questions = quiz.questions;
        let background_color = quiz.color;
        let options = questions[currentQuestion].questionOptions;
        
        VStack(alignment: .center) {
            Spacer()
            Text(quiz.name)
                .font(.caption)
                .foregroundStyle(.gray)
            Text("Question \(currentQuestion + 1) ot of \(quiz.numQuestions)")
                .foregroundStyle(.gray)
                .bold()
            
            Text(questions[currentQuestion].text)
                .foregroundStyle(.white)
                .font(.title)
                .bold()
                .multilineTextAlignment(.center)
            
            VStack(alignment: .leading, content: {
                ForEach(options, id: \.self) { option in
                    VStack(alignment: .center) {
                        Text(option.text)
                            .frame(maxWidth: .infinity)
                            .padding()
                    }
                    .frame(width: .infinity)
                    .background(
                        getBackground(selectedOption, option.option, questions[currentQuestion].answer)
                    )
                    .onTapGesture {
                        guard let selected = selectedOption else {
                            selectedOption = option.option;
                            if (selectedOption == questions[currentQuestion].answer) {
                                rightAnswers += 1;
                            }
                            return
                        }
                    }
                }
            })
            .frame(width: .infinity)
            .padding(.vertical, 20)
            .padding(.horizontal, 5)
            
            Spacer()
            
            if (selectedOption != nil) {
                if (currentQuestion == quiz.numQuestions - 1) {
                    Button(action: {
                        coordinator.push(.quizFinished(params: QuizFinshedParams(quiz: quiz, rightAnswers: rightAnswers)))
                    }, label: {
                        Text("End quiz")
                            .padding()
                            .foregroundStyle(.white)
                            .font(.title)
                    })
                    
                } else {
                    Button(action: {
                        currentQuestion += 1;
                        selectedOption = nil;
                    }, label: {
                        Text("Next question")
                            .padding()
                            .foregroundStyle(.white)
                            .font(.title)
                    })
                }
                Spacer().frame(height: 10)
            } else {
                Text("placeholder")
                    .padding()
                    .foregroundStyle(.indigo)
                    .font(.title)
                    .fontWeight(.bold)
            }
        }
        .frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, maxWidth: .infinity, maxHeight: .infinity)
        .background(
            .indigo
        )
        
    }
    
}

func getBackground(_ selectedAnswer : ClosedQuestionAnswer?,
                   _ currentOption: ClosedQuestionAnswer,
                   _ rightAnswer : ClosedQuestionAnswer) -> some View {
    var cornersColor : Color = .gray;
    var backgroundColor : Color = .white
    guard let selected = selectedAnswer else {
        return RoundedRectangle(cornerRadius: 5)
            .fill(backgroundColor)
            .stroke(cornersColor, lineWidth: 1);
    }
    
    if (currentOption != selected) {
        backgroundColor = .white
    } else {
        if (selected == rightAnswer) {
            backgroundColor = .green.opacity(0.7)
            cornersColor = .green
        } else {
            backgroundColor = .red.opacity(0.7)
            cornersColor = .red
        }
    }
    
    return RoundedRectangle(cornerRadius: 5).fill(backgroundColor).stroke(cornersColor, lineWidth: 1);
}
