//
//  QuizOverviewView.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 15.03.2024.
//

import SwiftUI
import FirebaseFirestore
import Observation

struct QuizTile: View {
    let quiz: Quiz
    var body: some View {
        HStack {
            Rectangle()
                .fill( quiz.color)
                .frame(width: 50, height: 50)
                .overlay(
                    Image(systemName: quiz.icon)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .foregroundColor(.white)
                        .padding()
                )
                .padding(.trailing)
                .frame(alignment: .leading)
            
            VStack(alignment: .leading, content: {
                Text("\(quiz.name)")
                    .font(.headline)
                Text("\(quiz.numQuestions) questions")
                    .fontWeight(.light)
                    .foregroundStyle(.gray)
                Text("Author: \(quiz.author) ")
                    .fontWeight(.light)
                    .foregroundStyle(.gray)
            })
            
        }
        .padding(.horizontal)
        
    }
    
}

struct QuizListView: View {
    @FirestoreQuery(collectionPath: "quizes") var quizes: [Quiz];
    
    var body: some View {
        NavigationView {
            List {
                ForEach(quizes) { quiz in
                    NavigationLink(value: Screen.quizDetail(params: quiz)) {
                        QuizTile(quiz: quiz)
                    }
                }
            }
        }
        .navigationTitle("Quizzes")
    }
    
}
