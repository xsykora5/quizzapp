//
//  FallbackView.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 17.05.2024.
//

import SwiftUI

struct FallbackView: View {
    var body: some View {
        Text("There is nothing")
    }
}

#Preview {
    FallbackView()
}
