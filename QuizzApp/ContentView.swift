//
//  ContentView.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 11.03.2024.
//

import SwiftUI
import FirebaseAuth

struct ContentView: View {
    @State private var username: String = ""
        @State private var password: String = ""
        
        var body: some View {
            VStack {
                Text("Login")
                    .font(.largeTitle)
                    .fontWeight(.bold)
                    .padding(.bottom, 20)
                
                TextField("Username", text: $username)
                    .padding()
                    .background(Color(.secondarySystemBackground))
                    .cornerRadius(8)
                    .padding(.horizontal)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                
                SecureField("Password", text: $password)
                    .padding()
                    .background(Color(.secondarySystemBackground))
                    .cornerRadius(8)
                    .padding([.horizontal, .bottom])
                
                Button(action: {
                    // Perform login action here
                    // For demonstration, let's just print the username and password
                    print("Username: \(username)")
                    print("Password: \(password)")
                    Auth.auth().signIn(withEmail: username, password: password) { authResult, error in
                        print(error)
                        print(authResult?.user)
                        print(authResult)
                    }
                }) {
                    Text("Sign In")
                        .foregroundColor(.white)
                        .padding()
                        .frame(maxWidth: .infinity)
                        .background(Color.blue)
                        .cornerRadius(8)
                        .padding(.horizontal)
                }
                
                Spacer()
            }
            .padding()
        }
}

#Preview {
    ContentView()
}
