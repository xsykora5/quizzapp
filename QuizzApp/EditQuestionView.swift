//  Created by Vojtěch Sýkora on 13.05.2024.
//

import SwiftUI

struct EditQuestionView: View {
    
    let order: Int
    @Binding var question : Question
    
    var body: some View {
        @State var options = $question.questionOptions;
        Section(header: Text("Question number \(order)")) {
            TextField("Question text", text: $question.text)
            Text("Options")
            ForEach(options, id: \.self) { option in
                TextField("Option \(option.option) text", text: option.text)
            }
            Text("Right answer")
            Picker("Right answer", selection: $question.answer) {
                ForEach(ClosedQuestionAnswer.allCases, id: \.self) { option in
                    Text(option.rawValue).tag(option)
                }
            }.pickerStyle(.segmented).colorMultiply(.indigo)
        }
        
    }
}
