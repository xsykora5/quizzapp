//
//  ServiceContext.swift
//  QuizzApp
//
//  Created by Marek Seďa on 08.05.2024.
//

import Foundation
import FirebaseCore

class ServiceContext {
    var authenticationService: AuthenticationService
    var databaseService: DatabaseService
    
    init() {
        FirebaseApp.configure()
        self.authenticationService = AuthenticationService()
        self.databaseService = DatabaseService()
    }
}

let serviceContext: ServiceContext = ServiceContext()
