//
//  DatabaseService.swift
//  QuizzApp
//
//  Created by Marek Seďa on 08.05.2024.
//

import Foundation
import FirebaseFirestore
import FirebaseAuth

class DatabaseService {
    let db = Firestore.firestore()

    func getUserQuizzes(userId: String) async -> [Quiz] {
        do {
            return try await db.collection("quizes").whereField("authorId", isEqualTo: userId).getDocuments().documents.map { try $0.data(as: Quiz.self) }
        } catch {
            return []
        }
    }
    
    func addQuiz(quiz: Quiz) -> Quiz {
        do {
          try db.collection("quizes").addDocument(from: quiz)
        } catch let error {
          print("Error writing quiz to Firestore: \(error)")
        }
        return quiz;
    }
    
    func deleteQuiz(id: String) {
        do {
            try db.collection("quizes").document(id).delete()
        } catch let error {
            print(error)
        }
         
    }
    
    func updateQuiz(quiz: Quiz) -> Quiz {
        do {
            try db.collection("quizes").document(quiz.id!).setData(from: quiz)
        } catch let error {
          print("Error writing quiz to Firestore: \(error)")
        }
        return quiz;
    }
    
    func addUser(user: User) -> User {
        do {
            try db.collection("users").document(user.email).setData(from: user)
        } catch let error {
          print("Error writing user to Firestore: \(error)")
        }
        return user;
    }
    
    func updateUser(user: User) -> User {
        do {
            try db.collection("users").document(user.email).setData(from: user)
        } catch let error {
          print("Error writing user to Firestore: \(error)")
        }
        return user;
    }
    
    func getLoggedUser() async -> User {
        do {
            let user = try await db.collection("users").document(Auth.auth().currentUser!.email!).getDocument(as: User.self)
            return user
        } catch {
            return User(
            name: "Fallback user",
            email: "test@test.cz",
            score: 23
            )
        }
    }
    
    func completeQuiz(quizCompleted: QuizCompleted) async -> QuizCompleted {
        do {
            try db.collection("completed-quizes").addDocument(from: quizCompleted)
            var user = try await db.collection("users").document(Auth.auth().currentUser!.email!).getDocument(as: User.self)
            user.score = user.score + quizCompleted.score
            try db.collection("users").document(user.email).setData(from: user)
        } catch let error {
          print("Error writing completed-quiz to Firestore: \(error)")
        }
        return quizCompleted;
    }
    
    func getUserStatistic() async -> UserStatistic {
        do {
            let user = try await db.collection("users").document(Auth.auth().currentUser!.email!).getDocument(as: User.self)
            let completedQuizes = try await db.collection("completed-quizes").whereField("playerId", isEqualTo: user.id).getDocuments().count
            let createdQuizes = try await db.collection("quizes").whereField("authorId", isEqualTo: user.id).getDocuments().count
            return UserStatistic(numberOfCompletedQuizes: completedQuizes, score: user.score, numberOfCreatedQuizes: createdQuizes)
        } catch {
            return UserStatistic(numberOfCompletedQuizes: 7, score: 25, numberOfCreatedQuizes: 2)
        }
    }
}
