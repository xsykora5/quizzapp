//
//  AuthenticationService.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 03.05.2024.
//

import Foundation
import Firebase

class AuthenticationService {
    init() {
    }
    
    func isSignedId() -> Bool {
        return Auth.auth().currentUser != nil
    }

  
    func signUp(email: String, password: String, completion: @escaping (AuthDataResult?, Error?) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) { result, error in
            completion(result, error)
        }
    }

    func signIn(email: String, password: String, completion: @escaping (AuthDataResult?, Error?) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { result, error in
            completion(result, error)
        }
    }

    func signOut() {
        do {
            try Auth.auth().signOut()
        } catch let error {
            
        }
    }
}
