//
//  QuizFinishedView.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 29.04.2024.
//

import SwiftUI

struct QuizFinishedView: View {
    var body: some View {
        
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

#Preview {
    QuizFinishedView()
}
