//
//  PlayerMainPageView.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 11.03.2024.
//

import SwiftUI

struct HomeView: View {
    var body: some View {
        TabView {
            Group {
                QuizListView()
                    .tabItem({ Image(systemName: "gamecontroller")
                        Text("Quizes")})
                    .tag(0)
                RankingsView()
                    .font(.title)
                    .tabItem({ Image(systemName: "list.number")
                        Text("Rankings") })
                    .tag(1)
                PlayerView()
                    .font(.title)
                    .tabItem({ Image(systemName: "person")
                        Text("My account") })
                    .tag(2)
                CreateNewQuizView()
                    .tabItem({ Image(systemName: "plus")
                        Text("New quiz")})
                    .tag(3)
            }
        }
    }
}

struct TileView: View {
    var description: String
    
    var body: some View {
        HStack {
            Image(systemName: "play.fill")
                .foregroundColor(.white)
                .padding()
            Text(description)
                .font(.headline)
                .foregroundColor(.white)
                .frame(maxWidth: .infinity, alignment: .leading)
            
        }
        
        .background(Color.primary)
        .cornerRadius(10)
        .frame(maxWidth: .infinity)
        .padding()
        
    }
}
