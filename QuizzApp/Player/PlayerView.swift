//
//  PlayerView.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 18.05.2024.
//

import SwiftUI

struct PlayerView: View {
    @State var activeSheet : Bool = false;
    
    @State var player: User? = nil
    @State var statistic: UserStatistic? = nil
    @State var quizes: [Quiz] = []
    
    var body: some View {
        VStack() {
            HStack() {
                Image(systemName: "gearshape.fill").foregroundColor(.indigo)
                Spacer()
                Text("Profile")
                    .font(.title)
                    .fontWeight(.bold)
                    .foregroundStyle(.white)
                
                Spacer()
                
                Image(systemName: "gearshape.fill").foregroundColor(.white)
                    .onTapGesture {
                        activeSheet.toggle()
                    }
            }.padding(.horizontal)
            Spacer().frame(height: 50)
           
            VStack() {
                Spacer().frame(height: 20)
                Text(player?.name ?? "")
                    .font(.title)
                    .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                Text(player?.email ?? "")
                    .foregroundStyle(.gray)
                Spacer()
                Text("My statistics")
                    .font(.title)
                Grid {
                    GridRow {
                        buildTile(image: "chart.bar", info: statistic?.score.formatted() ?? "0", caption: "score")
                        buildTile(image: "gamecontroller", info: statistic?.numberOfCompletedQuizes.formatted() ?? "0", caption: "games played")
                        buildTile(image: "number.square", info: statistic?.numberOfCreatedQuizes.formatted() ?? "0", caption: "quizzes created")
                    }
                }
                .padding()
                .frame(maxHeight: 200)
                Spacer()
                            
                if (player != nil) {
                    NavigationLink(value: Screen.playerQuizzes(params: player!.id!)) {
                        Text("Manage my quizzes")
                            .font(.title3)
                        .padding(10)
                        .foregroundStyle(.white)
                        .frame(maxWidth: .infinity)
                        .background(.indigo)
                        .cornerRadius(5.0)
                        .padding(.horizontal, 75)
                    }
                }
                
                Spacer()
                
                Button("Logout") {
                    serviceContext.authenticationService.signOut()
                }
                .foregroundStyle(.red)
                .font(.title3)
            }
            .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, maxHeight: .infinity)
            .cornerRadius(25.0)
            .background(RoundedRectangle(cornerRadius: 25.0).fill(.white).edgesIgnoringSafeArea(.bottom))
        }.onAppear {
            Task {
                var loggedPlayer = await serviceContext.databaseService.getLoggedUser();
                player = loggedPlayer
                statistic = await serviceContext.databaseService.getUserStatistic()
                quizes = await serviceContext.databaseService.getUserQuizzes(userId: loggedPlayer.id!)
            }
        }
        .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, maxHeight: .infinity)
        .background(Color.indigo.edgesIgnoringSafeArea(.all))
        .sheet(isPresented: $activeSheet, content: {
           
            UserSettingsSheet(user: player!)
        })
    }
}

@ViewBuilder
func buildTile(image: String, info: String, caption: String) -> some View {
    VStack(alignment: .center) {
        Image(systemName: image)
            .font(.system(size: 25))
            .padding(.bottom, 5)
        Text(info)
            .foregroundStyle(.indigo)
            .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
        Text(caption)
            .font(.caption)
    }
    .padding()
    .frame(maxHeight: .infinity)
    .frame(maxWidth: .infinity)
    .background(
        RoundedRectangle(cornerRadius: 15)
            .stroke(Color.gray, lineWidth: 1)
    )
}
