////
////  PlayerQuizzesView.swift
////  QuizzApp
////
////  Created by Vojtěch Sýkora on 18.05.2024.
////
//
//import SwiftUI
//
//struct PlayerQuizzesView: View {
//    
//    @Binding var quizes : [Quiz];
//    var body: some View {
//        ScrollView(.horizontal) {
//            LazyHStack {
//                ForEach(quizes) { quiz in
//                    NavigationLink(value: Screen.updateQuizView(params: quiz)) {
//                        PlayerQuizTile(quiz: quiz)
//                    }
//                }
//            }
//            .frame(alignment: .leading)
//            .padding()
//        }
//    }
//}
//
//struct PlayerQuizTile: View {
//    @Binding var quiz: Quiz
//    var body: some View {
//        VStack(alignment: .center) {
//            Rectangle()
//                .fill(quiz.color)
//                .frame(width: 50, height: 50)
//                .overlay(
//                    Image(systemName: quiz.icon)
//                        .resizable()
//                        .aspectRatio(contentMode: .fit)
//                        .foregroundColor(.white)
//                        .padding()
//                )
//                .frame(alignment: .leading)
//            Text(quiz.name)
//                .fontWeight(.thin)
//                .foregroundStyle(.black)
//        }
//        .padding()
//        .frame(width: 125, height: 125)
//
//        
//    }
//    
//}
//
//
//#Preview {
//    PlayerQuizzesView(quizes: sampleQuiz)
//}
