//
//  UserSettingsSheet.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 18.05.2024.
//

import SwiftUI

struct UserSettingsSheet: View {
    @Environment(\.dismiss) var dismiss
    
    @State var user : User
    var body: some View {
        Form() {
            Section() {
                VStack(alignment: .leading) {
                    Text("Player name").font(.caption).foregroundStyle(.indigo)
                    TextField("User name", text: $user.name)
                }
                
                VStack(alignment: .leading) {
                    Text("Email").font(.caption).foregroundStyle(.indigo)
                    TextField("User name", text: $user.email)
                }
            }
            
            Section {
                Button("Save changes") {
                    let _ = serviceContext.databaseService.updateUser(user: user)
                    dismiss()
                }
                .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/)
                .font(.title3)
            }
            
            Section {
                Button("Discard changes") {
                    dismiss()
                }
                .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/)
                .font(.title3)
                .foregroundStyle(.red)
            }
            
            
            

        }
    }
}
