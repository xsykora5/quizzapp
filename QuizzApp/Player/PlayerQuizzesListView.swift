//
//  PlayerQuizzesListView.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 19.05.2024.
//

import SwiftUI
import FirebaseFirestore
import Observation

struct PlayerQuizzesListView: View {
    @FirestoreQuery(collectionPath: "quizes") var quizes: [Quiz];
    var author : String;
    
    var body: some View {
        List {
            ForEach(quizes.filter{$0.authorId == author}) { quiz in
                NavigationLink(value: Screen.updateQuizView(params: quiz)) {
                    QuizTile(quiz: quiz)
                }
            }.onDelete(perform: { indexSet in
                removeRows(at: indexSet)
            })
        }
        .navigationTitle("My Quizzes")
        .frame(alignment: .leading)
    }
    
    func removeRows(at offsets: IndexSet) {
        offsets.forEach { (i) in
            serviceContext.databaseService.deleteQuiz(id: quizes.filter{$0.authorId == author}[i].id!)
        }
    }
}
