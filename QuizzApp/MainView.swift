//
//  MainView.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 17.05.2024.
//

import SwiftUI
import Observation

struct MainView: View {
    @State private var coordinator = NavigationCoordinator()
    
    var body: some View {
        NavigationStack(path: $coordinator.paths) {
            coordinator.navigate(to: .homeScreen)
            // navigate depending on the screen added to paths property in the coordinator
            .navigationDestination(for: Screen.self) { screen in
                coordinator.navigate(to: screen)
            }
        }
        // inject object into environment so there is no need to pass to each view
        .environment(coordinator)
    }
}

#Preview {
    MainView()
}
