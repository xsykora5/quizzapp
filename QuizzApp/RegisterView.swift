//
//  RegisterView.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 18.05.2024.
//

import SwiftUI

struct RegisterView: View {
    @State var username : String = "";
    @State var email : String = "";
    @State var password : String = "";
    @State var showAlert : Bool = false;
    @State private var alertMessage: String = ""
    var body: some View {
        VStack() {
            Spacer().frame(height: 20)
            
                Text("Sign up now")
                    .font(.title)
                    .fontWeight(.bold)
                    .foregroundStyle(.white)
                
            
            Text("to test your knowledge!").font(.caption).foregroundStyle(.white)
            Spacer().frame(height: 30)
           
            VStack() {
                
                Form() {
                    VStack(alignment: .leading) {
                        Text("Display name").font(.caption).foregroundStyle(.indigo)
                        TextField("User name", text: $username)
                    }
                    
                    VStack(alignment: .leading) {
                        Text("Email").font(.caption).foregroundStyle(.indigo)
                        TextField("Email", text: $email)
                    }
                    
                    VStack(alignment: .leading) {
                        Text("Password").font(.caption).foregroundStyle(.indigo)
                        SecureField("Password", text: $password)
                    }
                }
                .background(Color.gray.opacity(0.15))
                .scrollContentBackground(.hidden)
                Spacer()
                HStack() {
                    Text("Sign up").padding(.vertical, 10)
                }
                .foregroundStyle(.white)
                .frame(maxWidth: .infinity)
                .background(.indigo)
                .cornerRadius(5.0)
                .padding(.horizontal, 75)
                .onTapGesture {
                    if (username.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty) {
                        showAlert = true;
                        alertMessage = "Name is required"
                        return;
                    }
                    
                    let user : User = User(name: username, email: email, score: 0);
                    
                    serviceContext.authenticationService.signUp(email: email, password: password) { (result, error) in
                        if let error = error {
                            alertMessage = "Failed to register account " + error.localizedDescription
                            showAlert.toggle();
                        } else {
                            email = "";
                            password = "";
                            serviceContext.databaseService.addUser(user: user);
                            
                            serviceContext.authenticationService.signIn(email: email, password: password) { (result, error) in
                                if let error = error {
                                    alertMessage = "Failed to log in " + error.localizedDescription
                                    showAlert = true
                                } else {
                                    // should be redirected
                                }
                            }
                        }
                    }
                    
                    
                    
                    
                }
            }
            .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, maxHeight: .infinity)
            .cornerRadius(25.0)
            .background(RoundedRectangle(cornerRadius: 25.0).fill(.white).edgesIgnoringSafeArea(.bottom))
        }
        .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, maxHeight: .infinity)
        .background(Color.indigo.edgesIgnoringSafeArea(.all))
        .alert(isPresented: $showAlert, content: {
            Alert(
                title: Text("Register account"),
                message: Text(alertMessage)
            )
        })
    }
}

#Preview {
    RegisterView()
}
