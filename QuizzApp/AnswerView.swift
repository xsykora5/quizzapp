//
//  AnswerView.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 28.04.2024.
//

import SwiftUI

struct AnswerView: View {
    let question : Question;
    let accentColor: Color;
    @State private var selectedOption : ClosedQuestionAnswer?;
    var body: some View {
        // TODO : call firebase here
        let options = question.questionOptions
        VStack() {
            Text(question.text)
                .font(.title)
                .padding()
                
        }
        .frame(width: .infinity)
        .background( RoundedRectangle(cornerRadius: 20.0)
            .fill(.white)
            .opacity(0.25)
            .shadow(radius: 10.0)
        )
        Spacer().frame(height: 40)
        VStack(alignment: .leading, content: {
            ForEach(options, id: \.self) { option in
                if (option.option == selectedOption) {
                    HStack(alignment: .center) {
                        Rectangle()
                            .fill(accentColor)
                            .frame(width: 50, height: 50)
                            .overlay(
                                Text(option.option.rawValue)
                                    .font(.title)
                                    .foregroundStyle(.white)
                               
                            )
                            .frame(alignment: .leading)
                            .cornerRadius(10)
                        Text(option.text).foregroundStyle(accentColor).fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                        Spacer()
                    }
                    .frame(minWidth: 200, maxWidth: .infinity)
                    .background(.white)
                    .cornerRadius(10)
                    .contentShape(Rectangle())
                    .onTapGesture {
                        selectedOption = option.option;
                    }
                 
                } else {
                    HStack(alignment: .center) {
                        Rectangle()
                            .fill(Color.black)
                            .frame(width: 50, height: 50)
                            .overlay(
                                Text(option.option.rawValue)
                                    .font(.title)
                                    .foregroundStyle(.white)
                               
                            )
                            .frame(alignment: .leading)
                            .cornerRadius(10)
                        Text(option.text)
                        Spacer()
                    }
                    .frame(minWidth: 200, maxWidth: .infinity)
                    .contentShape(Rectangle())
                    .onTapGesture {
                        selectedOption = option.option;
                    }
                    
                }
                
                
            }
        })
        .padding()
        .background( RoundedRectangle(cornerRadius: 20.0)
            .fill(.white)
            .opacity(0.25)
            .shadow(radius: 10.0)
        )
        
    }
}
