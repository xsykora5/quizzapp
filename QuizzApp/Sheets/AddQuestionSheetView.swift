//
//  AddQuestionSheetView.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 16.05.2024.
//

import SwiftUI

struct AddQuestionSheetView: View {
    @Environment(\.dismiss) var dismiss
    
    @Binding var questions : [Question];
    @State var questionText: String = ""
    @State var optionAText: String = ""
    @State var optionBText: String = ""
    @State var optionCText: String = ""
    @State var optionDText: String = ""
    @State var rightAnswer: ClosedQuestionAnswer = ClosedQuestionAnswer.A

    var body: some View {
        Form {
            Section(header: Text("Add new question")) {
                TextField("Question text", text: $questionText)
                Text("Options")
                TextField("Option A text", text: $optionAText)
                TextField("Option B text", text: $optionBText)
                TextField("Option C text", text: $optionCText)
                TextField("Option D text", text: $optionDText)
                Text("Right answer")
                Picker("Right answer", selection: $rightAnswer) {
                    ForEach(ClosedQuestionAnswer.allCases, id: \.self) { option in
                        Text(option.rawValue).tag(option)
                    }
                }.pickerStyle(.segmented).colorMultiply(.indigo)
                Spacer()
                Button("Save") {
                    var newQuestion = Question(text: questionText, answer: rightAnswer)
                    newQuestion.questionOptions = [
                        QuestionOption(questionId: newQuestion.id, text: optionAText, option: .A),
                        QuestionOption(questionId: newQuestion.id, text: optionBText, option: .B),
                        QuestionOption(questionId: newQuestion.id, text: optionCText, option: .C),
                        QuestionOption(questionId: newQuestion.id, text: optionDText, option: .D),
                    ]
                                                                                          
                    questions.append(newQuestion);
                    dismiss();
                }
                .font(.title)
            }
            
        }
       
    }
}
