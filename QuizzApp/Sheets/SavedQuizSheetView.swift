//
//  SavedQuizSheetView.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 17.05.2024.
//

import SwiftUI

struct SavedQuizSheetView: View {
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        VStack() {
            Spacer()
            Image(systemName: "checkmark.circle")
                .imageScale(.large)
                .font(.system(size: 60))
                .foregroundColor(.green)
            Text("Quiz saved!")
                .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
            Spacer()
            Button("Dismiss") {
                dismiss()
            }
            .font(.title)
        }
    }
}

#Preview {
    SavedQuizSheetView()
}
