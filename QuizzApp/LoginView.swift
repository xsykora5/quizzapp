//
//  LoginView.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 11.03.2024.
//

import SwiftUI

struct LoginView: View {
    @State private var email: String = ""
    @State private var password: String = ""
    @State private var showAlert: Bool = false
    @State private var alertMessage: String = ""
    var body: some View {
        NavigationStack() {
            VStack {
                Text(String(localized: "login"))
                    .font(.largeTitle)
                    .fontWeight(.bold)
                Image(.logoQuiz)
                
                    .padding(.bottom, 20)
                TextField(String(localized: "email"), text: $email)
                    .padding()
                    .background(Color(.secondarySystemBackground))
                    .cornerRadius(8)
                    .padding(.horizontal)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                
                SecureField(String(localized: "password"), text: $password)
                    .padding()
                    .background(Color(.secondarySystemBackground))
                    .cornerRadius(8)
                    .padding([.horizontal, .bottom])
                
                Button(action: {
                    
                    serviceContext.authenticationService.signIn(email: email, password: password) { (result, error) in
                        if let error = error {
                            alertMessage = "Failed to log in " + error.localizedDescription
                            showAlert = true
                        } else {
                            // should be redirected
                        }
                    }
                }) {
                    Text(String(localized:"sign_in"))
                        .foregroundColor(.white)
                        .padding()
                        .frame(maxWidth: .infinity)
                        .background(Color.blue)
                        .cornerRadius(8)
                        .padding(.horizontal)
                }
                
                Spacer()
                Text("Dont have an account yet?")
                    .font(.caption)
                    .foregroundColor(Color.blue)
                
                NavigationLink(destination: {
                    RegisterView()
                }, label: {
                    Text("Register")
                        .foregroundColor(Color.blue)
                        .padding()
                        .frame(maxWidth: .infinity)
                        .background(Color(.secondarySystemBackground))
                        .cornerRadius(8)
                        .padding(.horizontal)
                })
            }
            .alert(isPresented: $showAlert, content: {
                Alert(
                    title: Text("Register account"),
                    message: Text(alertMessage)
                )
            })
            .padding()
        }
        
    }
    
}
