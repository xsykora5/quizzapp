//
//  QuizzAppApp.swift
//  QuizzApp
//
//  Created by Vojtěch Sýkora on 11.03.2024.
//

import SwiftUI
import FirebaseCore
import FirebaseFirestore
import FirebaseAuth


@main
struct QuizzAppApp: App {
    
    @State var isLoggedIn: Bool = false
    
    var body: some Scene {
        let _ = serviceContext.authenticationService.isSignedId(); // forces initialization
        var handle = Auth.auth().addStateDidChangeListener { auth, user in
            if (user != nil) {
                isLoggedIn = true
            } else {
                isLoggedIn = false
            }
        }

        WindowGroup {
            if isLoggedIn {
                MainView()
            } else {
                LoginView()
            }
        }
    }
}
